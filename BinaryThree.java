import java.util.*;

class BinaryThree<K, V> {
    Node root;
    
    public BinaryThree() {
        root = null;
    }
    public BinaryThree(K key, V value) {
        root = new Node<K, V>(key, value);
    }
    
    private Node addElement(K key, V value, Node current) {
        if(current == null) {
            return new Node(key, value);
        }
        
        if(key.toString() < current.getK()) {
            current.left = addElement(key, value, current.left);
        }
        
        if(key > current.key) {
            current.right = addElement(key, value, current.right);
        }
        
        if(key == current.key) {
            System.out.println("Element already exist");
            return current;
        }
        
        return current;
    }
    public void put(K key, V value) {
        root = addElement(key, value, root);
    }
  
    private void contLevelOrder(Node temp){
        Queue<Node> queue = new LinkedList<Node>();
        Node current = temp;
        int elementNumber = 1;
        int elementsNumber = 0;
        do {
            if(elementNumber == 0) {
                elementsNumber += 2;
                elementNumber = elementsNumber;
                System.out.println();
            }
            System.out.print(current.key + ":" + current.value);
            elementNumber--;
            if(!queue.isEmpty()) {
                queue.remove();
            }
            if (current.left != null) { 
                queue.add(current.left);
            }
            if (current.right != null) {
                queue.add(current.right);
            }
            if (!queue.isEmpty()) {
                current = queue.element();
            }
        } while (!queue.isEmpty());
    }
    public void print() {
        contLevelOrder(root);
        }
    
}