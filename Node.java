class Node<K, V> implements Comparable<Node<K, V>> {
    K key;
    V value;
    Node left;
    Node right;
    
    public Node(K key, V value) {
        left = null;
        right = null;
    }
    
    public String getK() {
        return key.toString();
    }

}